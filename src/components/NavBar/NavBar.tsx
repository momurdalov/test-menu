import React, { createRef, useState } from 'react';
import { navRouteList, IRouteListItem } from '@shared/consts/nav-route-list';
import Logo from '@assets/images/LogoDesktop.svg';
import ArrowLeft from '@assets/icons/Arrow-left.png';
import { RoutesList } from './components/RoutesList/RoutesList';
import styles from './NavBar.module.scss';
import menu from '@src/assets/icons/menu.png';
import closeBtn from '@src/assets/icons/closeBtn.png';
import { SelectLang } from './components/SelectLang/SelectLang';

export const NavBar: React.FC = () => {
  const [breadcrumbs, setBreadcrumbs] = useState<IRouteListItem[]>([]);
  const containerRef = createRef<HTMLDivElement>();
  const [showMenu, setShowMenu] = useState(false);

  function pushToBreadcrumbs(item: IRouteListItem) {
    setBreadcrumbs((prev) => {
      const arr = [...prev];
      arr.push(item);
      return arr;
    });
  }

  function goBack() {
    setBreadcrumbs((state) => {
      const prev = [...state];
      return prev.length > 1 ? prev.slice(0, prev.length - 1) : [];
    });
  }

  return (
    <div className={styles.navBarContainer}>
      <div ref={containerRef} className={`${styles.content} ${showMenu && styles.visible}`}>
        <div className={styles.selectorCont}>
          <div>
            <SelectLang />
          </div>
          <div className={styles.closeBtnCont} onClick={() => setShowMenu(false)}>
            <img src={closeBtn} className={styles.closeBtn} />
          </div>
        </div>

        <div className={`${styles.linksContainer} `}>
          <RoutesList
            onRouteClick={(route) => {
              setBreadcrumbs([]);

              if (route.nested) {
                pushToBreadcrumbs(route);
              } else {
                setShowMenu(false);
              }
            }}
            routes={navRouteList}
            chevronAlwaysTrue
          />

          <div className={styles.breadcrumbsContainer}>
            <div
              className={`${styles.breadcrumbList} ${styles.breadcrumbListMobile} ${
                breadcrumbs.length > 0 && styles.visible
              }`}
            >
              <img
                onClick={() => {
                  goBack();
                }}
                src={ArrowLeft}
                className={styles.backBtn}
              />
              {breadcrumbs.length > 0 && (
                <p className={`pt-mono ${styles.breadcrumb}`}>
                  {breadcrumbs[breadcrumbs.length - 1].alias}
                </p>
              )}
            </div>

            {breadcrumbs.length > 0 && (
              <RoutesList
                routes={breadcrumbs[breadcrumbs.length - 1].nested ?? []}
                onRouteClick={(route) => {
                  if (route.nested) {
                    pushToBreadcrumbs(route);
                  } else {
                    setShowMenu(false);
                  }
                }}
              />
            )}
          </div>
        </div>
      </div>

      <div className={styles.mobileNav}>
        <div
          className={`${styles.hamburger} mobile`}
          onClick={() => {
            setShowMenu((pr) => !pr);
          }}
        >
          <img className={styles.logo} src={menu} />
        </div>
      </div>
    </div>
  );
};
