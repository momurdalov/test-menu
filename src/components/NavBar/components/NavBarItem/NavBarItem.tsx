import React from 'react';
import ArrowRight from '@assets/icons/Arrow-right.png';
import { Link } from 'react-router-dom';
import styles from './NavBarItem.module.scss';

type NavBarItemProps = {
  alias: string;
  chevron?: boolean;
  title?: string;
  description?: string;
  contacts?: string;
  poisk?: string;
  onClick?: () => any;
};

export const NavBarItem: React.FC<NavBarItemProps> = ({
  onClick,
  alias,
  chevron,
  title,
  description,
  contacts,
  poisk,
}) => {
  return (
    <Link to={'#'} onClick={onClick}>
      <div className={styles.item}>
        {alias && <p className={styles.alias}>{alias}</p>}
        {description && (
          <div className={styles.desc}>
            <p className={styles.title}>{title}</p>
            <p className={styles.description}>{description}</p>
          </div>
        )}
        {contacts && (
          <div className={styles.contactsCont}>
            <p className={styles.contacts}>{contacts}</p>
          </div>
        )}
        {poisk && (
          <div className={styles.poiskCont}>
            <p className={styles.poisk}>{poisk}</p>
          </div>
        )}
        {chevron && <img className={styles.chevron} src={ArrowRight} />}
      </div>
    </Link>
  );
};
