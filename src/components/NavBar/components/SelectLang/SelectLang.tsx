import React, { Fragment, useState } from 'react';
import styles from './SelectLang.module.scss';
import arrowDown from '@assets/icons/arrowDown.png';
import check from '@src/assets/icons/check.png';
import { US, NE, BE, RU, KZ, TU } from '@assets/icons/icons';

interface ILangItem {
  id: number;
  imageSrc: string;
  selectedText: string;
  optionText: string;
}

const languages: ILangItem[] = [
  {
    id: 1,
    imageSrc: US,
    selectedText: 'US',
    optionText: 'United States',
  },
  {
    id: 2,
    imageSrc: NE,
    selectedText: 'NE',
    optionText: 'Netherlands',
  },
  {
    id: 3,
    imageSrc: BE,
    selectedText: 'BE',
    optionText: 'Беларусь',
  },
  {
    id: 4,
    imageSrc: RU,
    selectedText: 'RU',
    optionText: 'Россия',
  },
  {
    id: 5,
    imageSrc: KZ,
    selectedText: 'KZ',
    optionText: 'Казахстан',
  },
  {
    id: 6,
    imageSrc: TU,
    selectedText: 'TU',
    optionText: 'Türkiye',
  },
];

export const SelectLang: React.FC = () => {
  const [selectedItem, setSelectedItem] = useState<ILangItem>(languages[0]);
  const [showDropList, setShowDropList] = useState(false);

  return (
    <div className={styles.wrapper}>
      <div className={styles.container}>
        <div className={styles.container2}>
          <div onClick={() => setShowDropList(!showDropList)} className={styles.inputCont}>
            <img src={selectedItem.imageSrc} className={styles.image} />
            <p>{selectedItem.selectedText}</p>
            <img src={arrowDown} className={styles.arrowDown} />
          </div>
          {showDropList && (
            <div className={styles.dropList}>
              <div className={styles.dropListCont}>
                <p className={styles.title}>Страна</p>
                {languages.map((item) =>
                  item.id === selectedItem.id ? (
                    <div
                      className={styles.itemUse}
                      key={item.id}
                      onClick={() => {
                        setSelectedItem(item);
                        setShowDropList(false);
                      }}
                    >
                      <div className={styles.itemCont}>
                        <div className={styles.itemCont2}>
                          <img src={item.imageSrc} className={styles.imageSmall} />
                          <p>{item.optionText}</p>
                        </div>
                        <div>
                          <img src={check} className={styles.check} />
                        </div>
                      </div>
                    </div>
                  ) : (
                    <div
                      className={styles.itemsCont}
                      key={item.id}
                      onClick={() => {
                        setSelectedItem(item);
                        setShowDropList(false);
                      }}
                    >
                      <img src={item.imageSrc} className={styles.imageSmall} />
                      <p>{item.optionText}</p>
                    </div>
                  )
                )}
              </div>
            </div>
          )}
        </div>
        {showDropList && (
          <div
            className={styles.background}
            onClick={() => {
              setShowDropList(false);
            }}
          />
        )}
      </div>
    </div>
  );
};
