import React, { LegacyRef } from 'react';
import { IRouteListItem } from '@shared/consts/nav-route-list';
import { NavBarItem } from '../NavBarItem/NavBarItem';
import styles from './RoutesList.module.scss';

type RoutesListProps = {
  onRouteClick: (route: IRouteListItem) => any;
  routes: IRouteListItem[];
  chevronAlwaysTrue?: boolean;
};

export const RoutesList: React.FC<RoutesListProps> = (props) => {
  return (
    <div className={styles.links}>
      {props.routes?.map((route, i) => (
        <NavBarItem
          key={route.alias + i}
          alias={route.alias}
          title={route.title}
          description={route.description}
          contacts={route.contacts}
          poisk={route.poisk}
          onClick={() => {
            props.onRouteClick(route);
          }}
          chevron={!!(props.chevronAlwaysTrue ?? route.nested)}
        />
      ))}
    </div>
  );
};
