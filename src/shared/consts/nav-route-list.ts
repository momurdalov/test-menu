export interface IRouteListItem {
  alias: string;
  nested?: IRouteListItem[];
  title?: string;
  description?: string;
  contacts?: string;
  poisk?: string;
}

export const navRouteList: IRouteListItem[] = [
  {
    alias: 'Services',
    nested: [
      {
        alias: 'Облачные вычисления',
        nested: [
          {
            alias: '',
            title: 'Cloud consulting',
            description: 'Relational database services for MySQL, PostgreSQL, and SQL server.',
          },
          {
            alias: '',
            title: 'Cloud infrastructure analytics',
            description: 'Relational database services for MySQL, PostgreSQL, and SQL server.',
          },
          {
            alias: '',
            title: 'Hybrid Cloud',
            description: 'Health-specific solutions to enhance the patient experience.',
          },
          {
            alias: '',
            title: 'Hybrid Cloud',
            description: 'Data storage, AI, and analytics solutions for government agencies.',
          },
          {
            alias: '',
            title: 'Multicloud',
            description: 'Data storage, AI, and analytics solutions for government agencies.',
          },
          {
            alias: '',
            title: 'Hybrid Cloud',
            description: 'Relational database services for MySQL, PostgreSQL, and SQL server.',
          },
          {
            alias: '',
            title: 'Multicloud',
            description: 'Relational database services for MySQL, PostgreSQL, and SQL server.',
          },
          {
            alias: '',
            title: 'Multicloud',
            description: 'Data storage, AI, and analytics solutions for government agencies.',
          },
          {
            alias: '',
            title: 'Hybrid Cloud',
            description: 'Health-specific solutions to enhance the patient experience.',
          },
          {
            alias: '',
            title: 'Hybrid Cloud',
            description: 'Data storage, AI, and analytics solutions for government agencies.',
          },
          {
            alias: '',
            title: 'Multicloud',
            description: 'Data storage, AI, and analytics solutions for government agencies.',
          },
          {
            alias: '',
            title: 'Hybrid Cloud',
            description: 'Relational database services for MySQL, PostgreSQL, and SQL server.',
          },
          {
            alias: '',
            title: 'Multicloud',
            description: 'Relational database services for MySQL, PostgreSQL, and SQL server.',
          },
          {
            alias: '',
            title: 'Multicloud',
            description: 'Data storage, AI, and analytics solutions for government agencies.',
          },
          {
            alias: '',
            title: 'Hybrid Cloud',
            description: 'Data storage, AI, and analytics solutions for government agencies.',
          },
          {
            alias: '',
            title: 'Multicloud',
            description: 'Data storage, AI, and analytics solutions for government agencies.',
          },
          {
            alias: '',
            title: 'Hybrid Cloud',
            description: 'Relational database services for MySQL, PostgreSQL, and SQL server.',
          },
          {
            alias: '',
            title: 'Multicloud',
            description: 'Relational database services for MySQL, PostgreSQL, and SQL server.',
          },
          {
            alias: '',
            title: 'Multicloud',
            description: 'Data storage, AI, and analytics solutions for government agencies.',
          },
        ],
      },

      {
        alias: 'Выделенные серверы',
        nested: [
          {
            alias: '',
            title: 'Cloud consulting',
            description: 'Relational database services for MySQL, PostgreSQL, and SQL server.',
          },
          {
            alias: '',
            title: 'Cloud infrastructure analytics',
            description: 'Relational database services for MySQL, PostgreSQL, and SQL server.',
          },
          {
            alias: '',
            title: 'Hybrid Cloud',
            description: 'Health-specific solutions to enhance the patient experience.',
          },
          {
            alias: '',
            title: 'Hybrid Cloud',
            description: 'Data storage, AI, and analytics solutions for government agencies.',
          },
          {
            alias: '',
            title: 'Multicloud',
            description: 'Data storage, AI, and analytics solutions for government agencies.',
          },
          {
            alias: '',
            title: 'Hybrid Cloud',
            description: 'Relational database services for MySQL, PostgreSQL, and SQL server.',
          },
          {
            alias: '',
            title: 'Multicloud',
            description: 'Relational database services for MySQL, PostgreSQL, and SQL server.',
          },
          {
            alias: '',
            title: 'Multicloud',
            description: 'Data storage, AI, and analytics solutions for government agencies.',
          },
        ],
      },
      {
        alias: 'Платформенные сервисы',
        nested: [
          {
            alias: '',
            title: 'Cloud consulting',
            description: 'Relational database services for MySQL, PostgreSQL, and SQL server.',
          },
          {
            alias: '',
            title: 'Cloud infrastructure analytics',
            description: 'Relational database services for MySQL, PostgreSQL, and SQL server.',
          },
          {
            alias: '',
            title: 'Hybrid Cloud',
            description: 'Health-specific solutions to enhance the patient experience.',
          },
          {
            alias: '',
            title: 'Hybrid Cloud',
            description: 'Data storage, AI, and analytics solutions for government agencies.',
          },
          {
            alias: '',
            title: 'Multicloud',
            description: 'Data storage, AI, and analytics solutions for government agencies.',
          },
          {
            alias: '',
            title: 'Hybrid Cloud',
            description: 'Relational database services for MySQL, PostgreSQL, and SQL server.',
          },
          {
            alias: '',
            title: 'Multicloud',
            description: 'Relational database services for MySQL, PostgreSQL, and SQL server.',
          },
          {
            alias: '',
            title: 'Multicloud',
            description: 'Data storage, AI, and analytics solutions for government agencies.',
          },
        ],
      },
      {
        alias: 'Информационная безопасность',
        nested: [
          {
            alias: '',
            title: 'Cloud consulting',
            description: 'Relational database services for MySQL, PostgreSQL, and SQL server.',
          },
          {
            alias: '',
            title: 'Cloud infrastructure analytics',
            description: 'Relational database services for MySQL, PostgreSQL, and SQL server.',
          },
          {
            alias: '',
            title: 'Hybrid Cloud',
            description: 'Health-specific solutions to enhance the patient experience.',
          },
          {
            alias: '',
            title: 'Hybrid Cloud',
            description: 'Data storage, AI, and analytics solutions for government agencies.',
          },
          {
            alias: '',
            title: 'Multicloud',
            description: 'Data storage, AI, and analytics solutions for government agencies.',
          },
          {
            alias: '',
            title: 'Hybrid Cloud',
            description: 'Relational database services for MySQL, PostgreSQL, and SQL server.',
          },
          {
            alias: '',
            title: 'Multicloud',
            description: 'Relational database services for MySQL, PostgreSQL, and SQL server.',
          },
          {
            alias: '',
            title: 'Multicloud',
            description: 'Data storage, AI, and analytics solutions for government agencies.',
          },
        ],
      },
    ],
  },
  {
    alias: 'Managed IT',
    nested: [
      {
        alias: '',
        title: 'Cloud consulting',
        description: 'Relational database services for MySQL, PostgreSQL, and SQL server.',
      },
      {
        alias: '',
        title: 'Cloud infrastructure analytics',
        description: 'Relational database services for MySQL, PostgreSQL, and SQL server.',
      },
      {
        alias: '',
        title: 'Hybrid Cloud',
        description: 'Health-specific solutions to enhance the patient experience.',
      },
      {
        alias: '',
        title: 'Hybrid Cloud',
        description: 'Data storage, AI, and analytics solutions for government agencies.',
      },
      {
        alias: '',
        title: 'Multicloud',
        description: 'Data storage, AI, and analytics solutions for government agencies.',
      },
      {
        alias: '',
        title: 'Hybrid Cloud',
        description: 'Relational database services for MySQL, PostgreSQL, and SQL server.',
      },
      {
        alias: '',
        title: 'Multicloud',
        description: 'Relational database services for MySQL, PostgreSQL, and SQL server.',
      },
      {
        alias: '',
        title: 'Multicloud',
        description: 'Data storage, AI, and analytics solutions for government agencies.',
      },
    ],
  },
  {
    alias: 'Telecom Solutions',
    nested: [
      {
        alias: '',
        title: 'Cloud consulting',
        description: 'Relational database services for MySQL, PostgreSQL, and SQL server.',
      },
      {
        alias: '',
        title: 'Cloud infrastructure analytics',
        description: 'Relational database services for MySQL, PostgreSQL, and SQL server.',
      },
      {
        alias: '',
        title: 'Hybrid Cloud',
        description: 'Health-specific solutions to enhance the patient experience.',
      },
      {
        alias: '',
        title: 'Hybrid Cloud',
        description: 'Data storage, AI, and analytics solutions for government agencies.',
      },
      {
        alias: '',
        title: 'Multicloud',
        description: 'Data storage, AI, and analytics solutions for government agencies.',
      },
      {
        alias: '',
        title: 'Hybrid Cloud',
        description: 'Relational database services for MySQL, PostgreSQL, and SQL server.',
      },
      {
        alias: '',
        title: 'Multicloud',
        description: 'Relational database services for MySQL, PostgreSQL, and SQL server.',
      },
      {
        alias: '',
        title: 'Multicloud',
        description: 'Data storage, AI, and analytics solutions for government agencies.',
      },
    ],
  },
  {
    alias: 'About Us',
    nested: [
      {
        alias: '',
        title: 'Cloud consulting',
        description: 'Relational database services for MySQL, PostgreSQL, and SQL server.',
      },
      {
        alias: '',
        title: 'Cloud infrastructure analytics',
        description: 'Relational database services for MySQL, PostgreSQL, and SQL server.',
      },
      {
        alias: '',
        title: 'Hybrid Cloud',
        description: 'Health-specific solutions to enhance the patient experience.',
      },
      {
        alias: '',
        title: 'Hybrid Cloud',
        description: 'Data storage, AI, and analytics solutions for government agencies.',
      },
      {
        alias: '',
        title: 'Multicloud',
        description: 'Data storage, AI, and analytics solutions for government agencies.',
      },
      {
        alias: '',
        title: 'Hybrid Cloud',
        description: 'Relational database services for MySQL, PostgreSQL, and SQL server.',
      },
      {
        alias: '',
        title: 'Multicloud',
        description: 'Relational database services for MySQL, PostgreSQL, and SQL server.',
      },
      {
        alias: '',
        title: 'Multicloud',
        description: 'Data storage, AI, and analytics solutions for government agencies.',
      },
    ],
  },
  {
    alias: '',
    contacts: 'Контакты',
  },
  {
    alias: '',
    poisk: 'Поиск',
  },
];
