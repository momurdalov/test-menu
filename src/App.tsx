import React, { useEffect } from 'react';
import { NavBar } from '@src/components/NavBar/NavBar';
import { BrowserRouter } from 'react-router-dom';

const loader = document.getElementById('loader');

function App(): JSX.Element {
  useEffect(() => {
    loader?.classList.add('loader--hide');
  }, []);
  return (
    <BrowserRouter>
        <div className="app">
          <div className="content">
            <NavBar />
          </div>
        </div>
    </BrowserRouter>
  );
}

export default App;
